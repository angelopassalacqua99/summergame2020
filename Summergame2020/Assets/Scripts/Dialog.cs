﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Dialog: MonoBehaviour{

    
    public GameObject Panel;
    public TextMeshProUGUI textDisplay;
    public float typingSpeed;
    public IsoMCcontroller ComponentPlayer;
    public GameObject bottoneAvanti;
    public string[] sentences;
    private int index;
    
        void Start()
    {
         StartCoroutine(Type());
        
    }
        void Update()
    {
        if(textDisplay.text == sentences[index])
        {
            bottoneAvanti.SetActive(true);
        }
    }

    IEnumerator Type(){
        
        foreach (char letter in sentences[index].ToCharArray()){
            bottoneAvanti.SetActive(false);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);

        }
    }

    public void FraseDopo(){

        bottoneAvanti.SetActive(false);

        if (index < sentences.Length-1){
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }else 
        {
            Panel.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            ComponentPlayer.enabled=true;

        }
    
    }


}
