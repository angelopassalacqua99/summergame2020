﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GamePaused = false;
    public GameObject DiaManager;
    private bool DiaActive;
    public GameObject menuPausaUI;
    public GameObject menuOpzioniUI;

    void Start()
    {
        //Set Cursor to not be visible
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        DiaActive = DiaManager.activeSelf;
    }

    void Update()
    {
        

        if (Input.GetKeyDown(KeyCode.P)) {

            if (GamePaused==true)
            {
                Resume();
            }
            else if(GamePaused== false)
            {
                Pause();
            }
        }
    }

    


    public void Resume()
    {
        if (DiaManager.activeSelf == true)
        {
            DiaManager.SetActive(true);
        }
        menuPausaUI.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
        //Cursor not visible and locked
        Cursor.visible = false ;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Pause()
    {
        if (DiaManager.activeSelf == true)
        {
            DiaManager.SetActive(false);
        }
        menuPausaUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
        //cursor visible but confined
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void MenuIniz()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    public void optionMenu()
    {
        menuOpzioniUI.SetActive(true);
        menuPausaUI.SetActive(false);
    }


}
