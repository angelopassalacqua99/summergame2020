﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Audio;
using UnityEditor;

public class MenuManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    private GameObject GS;
    public void StatGame()
    {
        DontDestroyOnLoad(GameObject.Find("GLOBALSTUFF"));
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }

    public void Volume(float volumeSlide)
    {
        audioMixer.SetFloat("pippo", volumeSlide);
    }
}
