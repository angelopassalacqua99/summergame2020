﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSound : MonoBehaviour
{
    public AudioSource as_;
    public AudioClip SuonoPorta;



    void Awake()
    {
        as_ = GetComponent<AudioSource>();

    }
    IEnumerator Riproduci()
    {

        yield return null;
        as_.clip = SuonoPorta;
        as_.PlayOneShot(as_.clip);

    }
}
